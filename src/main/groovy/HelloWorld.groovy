import groovy.sql.Sql

class HelloWorld {
    static void main(String[] args) {
        println "Hello, World!"

        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/groovyDB',
                'postgres', 'postgres', 'org.postgresql.Driver')

        // Executing the query SELECT VERSION which gets the version of the database
        // Also using the eachROW method to fetch the result from the database

        sql.eachRow('SELECT VERSION()'){ row ->
            println row[0]
        }

        sql.close()
    }
}

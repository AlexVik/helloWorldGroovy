import org.flywaydb.core.Flyway
import org.springframework.beans.factory.InitializingBean

import javax.sql.DataSource
import java.sql.Connection
import java.sql.SQLException

class FlywayMigrationInitializer implements InitializingBean{
    DataSource dataSource;
    @Override
    void afterPropertiesSet() throws SQLException {
        String schema
		try (Connection connection = dataSource.getConnection()) {
			schema = connection.getSchema()
		}
        Flyway flyway = Flyway.configure()
				.dataSource(dataSource)
				.schemas(schema)
				.load()
		flyway.migrate()
	}

}

CREATE TABLE IF NOT EXISTS public."user"
(
    id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 32767 CACHE 1 ),
    name character varying(255) NOT NULL,
    profession character varying(255),
    recording_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT user_pkey PRIMARY KEY (id));